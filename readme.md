# Getting started

## Install required packages
1. Make sure you have the required python version (found in .python-version) if not: Install the required python version: for easy python version management, have a look at Pyenv.

    with pyenv: `pyenv install 3.8.2` & `pyenv global 3.8.2`
2.  install poetry for project package management:

    linux: `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python`
    windows (powershell): `(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -`
     
3. Once poetry is installed, cd into the project and install the dependencies:

    `cd ~/agrobot-docker` & `poetry install`
    
    